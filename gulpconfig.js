var config = {};

config.paths = {};
config.paths.basedir = global.process.env.INIT_CWD;
config.paths.tasks = config.paths.basedir + '/gulp-tasks';
config.paths.public = config.paths.basedir + '/public';
config.paths.node_modules = config.paths.basedir + '/node_modules';
config.paths.bower_components = config.paths.public + '/bower_components';
config.paths.sourcemaps = './';

config.paths.src = {
  styles  : config.paths.public + '/assets/src/styles',
  scripts : config.paths.public + '/assets/src/scripts',
  images  : config.paths.public + '/assets/src/images'
};

config.paths.dist = {
  styles  : config.paths.public + '/assets/dist/css',
  scripts : config.paths.public + '/assets/dist/js',
  images  : config.paths.public + '/assets/dist/img'
};

config.files = {
  jshintrc: config.paths.basedir + '/.jshintrc',
  src: {
    styles: {
      app: [
        config.paths.src.styles + '/app.sass'
      ],
      vendor: [
        config.paths.bower_components + '/normalize.css/normalize.css'
      ]
    },
    scripts: {
      app: [
        config.paths.src.scripts + '/app.babel.js'
      ],
      vendor: [
        config.paths.node_modules + '/babel-polyfill/dist/polyfill.js',
        config.paths.bower_components + '/fastclick/lib/fastclick.js',
        config.paths.bower_components + '/matchMedia/matchMedia.js',
        config.paths.bower_components + '/classlist/classList.js',
        config.paths.bower_components + '/closest/closest.js'
      ]
    }
  },
  dist: {
    styles: {
      app: 'app.css',
      vendor: 'vendor.css'
    },
    scripts: {
      app: 'app.js',
      vendor: 'vendor.js'
    }
  }
};

config.match = {
  dist: {
    styles: config.paths.dist.styles + '/**/*.css',
    scripts: config.paths.dist.scripts + '/**/*.js',
    images: config.paths.dist.images + '/**/*.{jpg,png,gif,svg}'
  },
  src: {
    styles: config.paths.src.styles + '/**/*.sass',
    scripts: config.paths.src.scripts + '/**/*.babel.js',
    images: config.paths.src.images + '/**/*.{jpg,png,gif,svg}'
  },
  views: config.paths.views + '/**/*.php'
};
config.plugins = {
  autoprefixer: {
    // List of browsers which are supported in your project
    browsers: [
      '> 1%',
      'last 2 versions',
      'Firefox ESR',
      'Explorer 9'
    ],
    cascade: true, // Use Visual Cascade if CSS is uncompressed
    add: true, // Add prefixes
    remove: true // Remove outdated prefixes
  },
  babel: {
    code: true, // Enable code generation
    compact: false, // Do not include superfluous whitespace characters and line terminators
    comments: true, // Output comments in generated output
    presets: ["es2015"] // List of presets to load and use
  },
  cssnano: {
    autoprefixer: false, // Parse CSS and add vendor prefixes
    discardComments: {
      removeAll: true // Remove all comments marked as important
    },
    discardDuplicates: true, // Discard duplicate rules in your CSS
    discardEmpty: true, // Discard empty rules and values
    discardUnused: {
      fontFace: true, // Pass false to disable discarding unused font face rules
      counterStyle: true, // Pass false to disable discarding unused counter style rules
      keyframes: true, // Pass false to disable discarding unused keyframe rules.
      namespace: true // Pass false to disable discarding unused namespace rules.
    }
  },
  imagemin: {
    optimizationLevel: 5, // Optimization level between 0 and 7
    progressive: true, // Lossless conversion to progressive
    interlaced: true, // Interlace gif for progressive rendering
    multipass: true // Optimize svg multiple times until it's fully optimized
  },
  livereload: {
    port: 35729, // Server port
    host: 'localhost', // Server host
    start: false, // Automatically start
    quiet: false // Disable console logging
  },
  plumber: {
    errorHandler: function (err) {
      var plugins = require(config.paths.node_modules + '/gulp-load-plugins')({
          config: config.paths.basedir + '/package.json'
        }),
        message = plugins.util.colors.red(err);
      plugins.util.beep();
      plugins.util.log(message);
      this.emit('end');
    }
  },
  rename: {
    suffix: '.min'
  },
  sass: {
    includePaths: [], // An array of paths that libsass can look in to attempt to resolve your @import declarations
    indentedSyntax: true, // Enable Sass Indented Syntax for parsing the data string or file
    indentType: 'space', // Used to determine whether to use space or tab character for indentation
    indentWidth: 2, // Used to determine the number of spaces or tabs to be used for indentation
    outputStyle: 'nested', // Determines the output format of the final CSS style (nested, expanded, compact, compressed)
    precision: 5, // Used to determine how many digits after the decimal will be allowed
    sourceComments: false // Enables additional debugging information in the output file as CSS comments
  },
  sourcemaps: {
    // addComment: true, // By default a comment referencing the source map is added. Set this to false to disable the comment
    // includeContent: false, // By default the source maps include the source code. Pass false to use the original files
    // sourceRoot: './source/' // Set the path where the source files are hosted
  },
  uglify: {
    mangle: true, // Pass false to skip mangling names
    // http://lisperator.net/uglifyjs/compress
    compress: {
      // sequences     : true,  // join consecutive statemets with the “comma operator”
      // properties    : true,  // optimize property access: a["foo"] → a.foo
      // dead_code     : true,  // discard unreachable code
      // drop_debugger : true,  // discard “debugger” statements
      // unsafe        : false, // some unsafe optimizations (see below)
      // conditionals  : true,  // optimize if-s and conditional expressions
      // comparisons   : true,  // optimize comparisons
      // evaluate      : true,  // evaluate constant expressions
      // booleans      : true,  // optimize boolean expressions
      // loops         : true,  // optimize loops
      // unused        : true,  // drop unused variables/functions
      // hoist_funs    : true,  // hoist function declarations
      // hoist_vars    : false, // hoist variable declarations
      // if_return     : true,  // optimize if-s followed by return/continue
      // join_vars     : true,  // join var declarations
      // cascade       : true,  // try to cascade `right` into `left` in sequences
      // side_effects  : true,  // drop side-effect-free statements
      // warnings      : true,  // warn about potentially dangerous optimizations/code
      // global_defs   : {}     // global definitions
    }
  },
  watch: {
    verbose: true, // Enable verbose output
    persistent: true, // Indicates whether the process should continue to run as long as files are being watched
    // Whether to use fs.watchFile (backed by polling), or fs.watch
    // It is typically necessary to set this to true to successfully watch files over a network
    usePolling: true
  }
};

module.exports = config;
