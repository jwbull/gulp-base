/* globals FastClick */

(()=> { 'use strict';

  // Cutting the Mustard
  // document.querySelector – A large part of any JS library is its DOM selector. If the browser has native CSS selecting then it removes the need for a DOM selector. QuerySelector has been available in Firefox since 3.5 at least and has been working in webkit for ages. It also works in IE9.
  // window.addEventListener – Another large part of any JS library is event support. Every browser made in the last 6 years (except IE8) supports DOM level 2 events. If the browser supports this then we know it has better standards support than IE8.
  if ('querySelector' in document && 'addEventListener' in window) {
    document.addEventListener('DOMContentLoaded', ()=> {

      FastClick.attach(document.body);

    });
  }

})();
