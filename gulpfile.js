'use strict';

var fs = require('fs');
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var basedir = global.process.env.INIT_CWD;
var config_file = basedir + '/gulpconfig.js';
var config;

let message = plugins.util.colors.blue('Basedir: ' + basedir);
plugins.util.log(message);

function getTask(task, done) {
  return require(config.paths.tasks + '/' + task)(gulp, plugins, config, done);
}

try {
  // does the configuration file exist?
  fs.statSync(config_file);
} catch (e) {
  if (e.code == 'ENOENT') {
    let message = plugins.util.colors.red('!!! No configuration file found !!!');
    plugins.util.log(message);
    let file = plugins.util.colors.yellow(config_file);
    plugins.util.log(file);
  }
}

// load configuration
config = require(config_file);
config.files.config = config_file;

// create gulp tasks once configuration has loaded
gulp.task('clean',             function (done) { return getTask('clean', done); });
gulp.task('images',            getTask('images'));
gulp.task('scripts-app',       getTask('scripts-app'));
gulp.task('scripts-vendor',    getTask('scripts-vendor'));
gulp.task('styles-app',        getTask('styles-app'));
gulp.task('styles-vendor',     getTask('styles-vendor'));
gulp.task('watch',             getTask('watch'));
gulp.task('scripts',           function (done) { runSequence(['scripts-vendor', 'scripts-app'], done); });
gulp.task('styles',            function (done) { runSequence(['styles-vendor', 'styles-app'], done); });
gulp.task('build',             function (done) { runSequence(['styles', 'scripts', 'images'], done); });
gulp.task('build-watch',       function (done) { runSequence('build', 'watch', done); });
gulp.task('clean-build',       function (done) { runSequence('clean', 'build', done); });
gulp.task('clean-build-watch', function (done) { runSequence('clean', 'build', 'watch', done); });
gulp.task('default',           function (done) { runSequence('build-watch', done); });
