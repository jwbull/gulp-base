module.exports = function (gulp, plugins, config) {
  return function () {
    return gulp.src(config.files.src.scripts.vendor)
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.concat(config.files.dist.scripts.vendor))
      .pipe(gulp.dest(config.paths.dist.scripts))
      .pipe(plugins.rename(config.plugins.rename))
      .pipe(plugins.uglify(config.plugins.uglify))
      .pipe(plugins.sourcemaps.write(config.paths.sourcemaps, config.plugins.sourcemaps))
      .pipe(gulp.dest(config.paths.dist.scripts));
  };
};
