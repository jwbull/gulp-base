module.exports = function (gulp, plugins, config) {
  return function () {
    // Start the livereload server
    plugins.livereload.listen(config.plugins.livereload);
    plugins.util.log('Livereload listening on port:', config.plugins.livereload.port);
    // Watch style assets to trigger build tasks
    plugins.watch(
      config.match.src.styles,
      config.plugins.watch,
      plugins.batch(function (events, done) {
        gulp.start('styles-app', done);
      }));
    // Watch script assets to trigger build tasks
    plugins.watch(
      config.match.src.scripts,
      config.plugins.watch,
      plugins.batch(function (events, done) {
        gulp.start('scripts-app', done);
      }));
    // Watch image assets to trigger build tasks
    plugins.watch(
      config.match.src.images,
      config.plugins.watch,
      plugins.batch(function (events, done) {
        gulp.start('images', done);
      }));
    // Watch build files to trigger livereload
    plugins.watch([
        config.match.dist.styles,
        config.match.dist.scripts,
        config.match.dist.images,
        config.match.views
      ],
      config.plugins.watch,
      function (file) {
        plugins.livereload.changed(file);
      });
  };
};
