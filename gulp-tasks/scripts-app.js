module.exports = function (gulp, plugins, config) {
  return function () {
    var jshintStylish = require('jshint-stylish');
    return gulp.src(config.files.src.scripts.app)
      .pipe(plugins.plumber(config.plugins.plumber))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.jshint(config.files.jshintrc))
      .pipe(plugins.jshint.reporter(jshintStylish))
      .pipe(plugins.concat(config.files.dist.scripts.app))
      .pipe(plugins.babel(config.plugins.babel))
      .pipe(gulp.dest(config.paths.dist.scripts))
      .pipe(plugins.rename(config.plugins.rename))
      .pipe(plugins.uglify(config.plugins.uglify))
      .pipe(plugins.sourcemaps.write(config.paths.sourcemaps, config.plugins.sourcemaps))
      .pipe(gulp.dest(config.paths.dist.scripts));
  };
};
