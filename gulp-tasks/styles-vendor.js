module.exports = function (gulp, plugins, config) {
  return function () {
    return gulp.src(config.files.src.styles.vendor)
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.concat(config.files.dist.styles.vendor))
      .pipe(gulp.dest(config.paths.dist.styles))
      .pipe(plugins.rename(config.plugins.rename))
      .pipe(plugins.cssnano(config.plugins.cssnano))
      .pipe(plugins.sourcemaps.write(config.paths.sourcemaps, config.plugins.sourcemaps))
      .pipe(gulp.dest(config.paths.dist.styles));
  };
};
