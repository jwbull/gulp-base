module.exports = function (gulp, plugins, config) {
  return function () {
    return gulp.src(config.match.src.images)
      .pipe(plugins.newer(config.paths.dist.images))
      .pipe(plugins.imagemin(config.imagemin))
      .pipe(gulp.dest(config.paths.dist.images));
  };
};
