module.exports = function (gulp, plugins, config) {
  return function () {
    return gulp.src(config.files.src.styles.app)
      .pipe(plugins.plumber(config.plugins.plumber))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.sass(config.plugins.sass))
      .pipe(plugins.autoprefixer(config.plugins.autoprefixer))
      .pipe(plugins.concat(config.files.dist.styles.app))
      .pipe(gulp.dest(config.paths.dist.styles))
      .pipe(plugins.rename(config.plugins.rename))
      .pipe(plugins.cssnano(config.plugins.cssnano))
      .pipe(plugins.sourcemaps.write(config.paths.sourcemaps, config.plugins.sourcemaps))
      .pipe(gulp.dest(config.paths.dist.styles));
  };
};
