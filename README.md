# Gulp Base
A gulp flow for incidental builds of your web assets.

## Tasks

* **clean**: Deletes all generated assets
* **images**: Will only build image assets
* **scripts**: Will only build script assets
* **styles**: Will only build style assets
* **build**: Will build all assets
* **watch**: Will listen for file changes and refresh the browser
* **default**: Will run build then watch

## Example
Run the following to install dependencies and build a test suite of assets.

```
$ npm install
$ cd example
$ bower install
$ node_modules/.bin/gulp
```
